package pt.zcarlos.smack.Model

// Só é necessário o construtor
class Message constructor(val message: String, val userName: String, val channelId: String,
                          val userAvatar: String, val userAvatarColor: String,
                          val id: String, val timeStamp: String)